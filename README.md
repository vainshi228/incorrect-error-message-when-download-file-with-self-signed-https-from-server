# Incorrect error message when download file with self-signed https from server

## How to make this error
1. Go to site with not secured  https
   
![](./imgs/1.png)

2. Send request with getting file from server

![](./imgs/2.png)

Site download file with the message "Network error" which 
is not correct for this situation.

It was testing on Windows/Linux, test table:

|           | google chrome | chromium     | mozilla firefox|
|-----------|---------------|--------------|----------------|
|Linux(http)| without error | without error| without error  |
|Linux(https not secured)| with error | with error| without error  |
|Linux(https secured)| without error | without error| without error  |
|Windows(http)| without error | -| without error  |
|Windows(https not secured)| with error | -| without error  |
|Windows(https secured)| without error | -| without error  |

## Example
I made example of server which make this exception
### Start
Install [node](https://nodejs.org/en/) last version

Download this repository and write:
```bash
cd path/to/repository
npm i
```

! Close all your working server on port 4000
#### Https
Start script:
```bash
npm run https
```
Go to page [https://localhost:4000](https://localhost:4000) on chroimum or google chrome

And click [file]()

You get the same error like this

![](./imgs/2.png)

If you import [`ca.pem`](./certs/ca.pem) to your browser
**Settings/Secure/Manage certificates/Authorities**
exception doesn't be 

Click [file]()
you get message like this:

![](./imgs/correct.png)

#### Http

Start script:
```bash
npm run https
```
Go to page [http://localhost:4000](http://localhost:4000) on chroimum or google chrome

And click [file]()

You get this file like in this photo

![](./imgs/correct.png)
