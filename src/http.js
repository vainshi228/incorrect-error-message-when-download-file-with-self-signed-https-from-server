const express = require('express')

const app = express()

const GET_FILE = '/get-file'
app.get('', (req, res) => {
    res.status(200).send(`<a href="${GET_FILE}" download="1.json">file</a>`)
})


app.get(GET_FILE, (req, res) => {
    return res.status(200).send(JSON.stringify({hello:'world'}))
})

app.listen(4000, () => {
    console.log('Server started')
})